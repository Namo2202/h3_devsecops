import streamlit as st
import pandas as pd
import os
from time import sleep
import plotly.graph_objs as go
from influxdb_client import InfluxDBClient

# Informations de connexion à InfluxDB
token = os.environ.get("INFLUXDB_TOKEN")
org = "H3 Hitema"
url = "http://localhost:8086"

# Création du client InfluxDB
client = InfluxDBClient(url=url, token=token, org=org)

# Sélection du bucket
bucket = "H3_system_metrics"

# Créer une page Streamlit
st.title("Monitoring du système")

# Définir une variable pour le temps de rafraîchissement
refresh_interval = 4  # en secondes

# Créer un élément vide pour le graphique
chart_placeholder = st.empty()

# Boucle pour rafraîchir le graphique
while True:
    # Récupérer les données des 10 dernières secondes depuis InfluxDB
    query_api = client.query_api()
    query = f"""
        from(bucket: "H3_system_metrics")
        |> range(start: -10m)
        |> filter(fn: (r) => r._measurement == "system_metrics")
    """
    tables = query_api.query(query)

    # Convertir les données en DataFrame pandas
    data = []
    for table in tables:
        for record in table.records:
            fields = record.values["_field"]
            time = pd.to_datetime(record.values["_time"])
            value = record.values["_value"]
            data.append((time, fields, value))
    df = pd.DataFrame(data, columns=["Time", "Field", "Value"])

    # Création d'une figure Plotly
    fig = go.Figure()

    # Parcourir tous les champs et tracer les données sur le graphique
    for field, df_field in df.groupby("Field"):
        fig.add_trace(go.Scatter(x=df_field["Time"], y=df_field["Value"], mode='lines', name=field))

    # Configurer la mise en page
    fig.update_layout(
        title="Monitoring du système",
        xaxis_title="Temps",
        yaxis_title="Valeur (%)",
        legend_title="Champs"
    )

    # Effacer le contenu précédent et afficher le nouveau graphique
    chart_placeholder.write(fig)

    # Attendre avant de rafraîchir à nouveau
    sleep(refresh_interval)
