import psutil
import influxdb_client
import os
import time
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

# Informations de connexion à InfluxDB
token = os.environ.get("INFLUXDB_TOKEN")
org = "H3 Hitema"
url = "http://localhost:8086"

# Création du client InfluxDB
client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

# Sélection du bucket
bucket = "H3_system_metrics"

# Boucle de collecte et d'écriture des données toutes les 2 secondes
while True:
    # Récupération des données métriques avec psutil
    cpu_percent = psutil.cpu_percent()
    memory_percent = psutil.virtual_memory().percent
    battery_percent = psutil.sensors_battery()
    disk_usage = psutil.disk_usage("/").percent
    # network_io_counters = psutil.net_io_counters()

    write_api = client.write_api(write_options=SYNCHRONOUS)

    # Création d'un point InfluxDB
    point = Point("system_metrics") \
        .tag("host", "localhost") \
        .field("cpu_percent", cpu_percent) \
        .field("ram_usage", memory_percent) \
        .field("battery_percent", battery_percent) \
        .field("disk_usage", disk_usage)
        # .field("network_bytes_sent", network_io_counters.bytes_sent) \
        # .field("network_bytes_recv", network_io_counters.bytes_recv)


    # Écriture du point dans le bucket
    write_api.write(bucket=bucket, org="H3 Hitema", record=point)

    print("Données écrites avec succès dans InfluxDB.")

    # Attente de 2 secondes avant la prochaine collecte de données
    time.sleep(5)
