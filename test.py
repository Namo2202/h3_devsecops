import pytest
import os
from influxdb_client import InfluxDBClient

@pytest.fixture(scope="session")
def influxdb_client():
    # Configuration de l'accès à InfluxDB
    token = os.environ.get("INFLUXDB_TOKEN")
    org = "H3 Hitema"
    url = "http://localhost:8086"
    bucket = "H3_system_metrics"

    # Création du client InfluxDB
    client = InfluxDBClient(url=url, token=token, org=org)

    # Renvoie du client pour l'utiliser dans les tests
    return client

def test_influxdb_connection(influxdb_client):
    # Vérifier que la connexion à InfluxDB fonctionne correctement
    assert influxdb_client is not None

def test_write_to_influxdb(influxdb_client):
    # Écrire des données fictives dans InfluxDB
    data = [
        {
            "measurement": "system_metrics",
            "tags": {"host": "localhost"},
            "fields": {
                "cpu_percent": 50,
                "memory_percent": 40,
                "battery_percent": 60,
                "disk_usage": 30
            }
        }
    ]
    write_api = influxdb_client.write_api()
    write_api.write(bucket="H3_system_metrics", record=data)

    # Vérifier que les données ont été écrites avec succès
    query = f'from(bucket: "H3_system_metrics") |> range(start: -1m)'
    tables = influxdb_client.query_api().query(query)
    assert len(tables) > 0
    assert len(tables[0].records) > 0
